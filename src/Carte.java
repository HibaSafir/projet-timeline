import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Carte {

	private String nom;
	private Double date, pollution, population, pib, superficie;
	private Double typeSelectionne;
	private Image imgRecto;
	private Image imgVerso;
	private int numero;

	/*
	 * Constructeur pour timeline
	 */
	public Carte(String n, double d, String img) throws IOException {
		this.nom = n;
		this.date = d;
		this.imgRecto = ImageIO.read(new File("data/timeline/cards/" + img + ".jpeg"));
		this.imgVerso = ImageIO.read(new File("data/timeline/cards/" + img + "_date.jpeg"));
		this.typeSelectionne = this.date;
	}

	/*
	 * Constructeur pour cardline
	 */
	public Carte(String n, double s, double pop, double pib, double pol, String img) throws IOException {
		this.nom = n;
		this.superficie = s;
		this.population = pop;
		this.pib = pib;
		this.pollution = pol;
		this.imgRecto = ImageIO.read(new File("data/cardline/cards/" + img + ".jpeg"));
		this.imgVerso = ImageIO.read(new File("data/cardline/cards/" + img + "_reponse.jpeg"));
		this.typeSelectionne = population;
	}

	public Carte(double d) {
		this.date = d;
		this.typeSelectionne = this.date;
	}

	/*
	 * M�thode qui permet de comparer deux cartes par rapport � leur date
	 * 
	 * @param la carte que l'on souhaite comparer
	 * 
	 * @return un entier indiquant si la date est sup�rieure, inf�rieure ou
	 * �gale
	 */
	public int comparerCartes(Carte carte) {
		return this.typeSelectionne.compareTo(carte.getTypeSelectionne());
	}

	@Override
	public String toString() {
		return "Carte [nom=" + nom + ", date=" + date + "]";
	}

	public Image getImgRecto() {
		return this.imgRecto;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Image getImgVerso() {
		return imgVerso;
	}

	public void setImgVerso(Image imgVerso) {
		this.imgVerso = imgVerso;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public void setImgRecto(Image imgRecto) {
		this.imgRecto = imgRecto;
	}

	public Double getTypeSelectionne() {
		return typeSelectionne;
	}

	public void setTypeSelectionne(String type) {
		switch (type) {
		case "pollution":
			this.typeSelectionne = this.pollution;
			break;
		case "population":
			this.typeSelectionne = this.population;
			break;
		case "superficie":
			this.typeSelectionne = this.superficie;
			break;
		case "pib":
			this.typeSelectionne = this.pib;
			break;
		}
	}
}
