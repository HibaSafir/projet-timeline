import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Deck {

	private ArrayList<Carte> cartes;

	public Deck() {
		this.cartes = new ArrayList<Carte>();
	}

	/*
	 * Methode qui permet de donner une carte al�atoire parmis celle existante et
	 * la supprime de la liste
	 * 
	 * @return une carte al�atoire de la liste
	 */
	public Carte piocherCarte() {
		int nombreCartes = this.cartes.size() - 1;
		int resultat = (int) Math.round(Math.random() * nombreCartes);
		Carte c = this.cartes.get(resultat);
		this.cartes.remove(resultat);
		return c;
	}

	/*
	 * M�thode qui g�n�re toutes les cartes pr�sentent dans le document csv, pour
	 * Timeline
	 */
	public void creerCartes(String jeu) throws IOException {
		long t1 = System.currentTimeMillis();
		BufferedReader br = new BufferedReader(new FileReader("data/" + jeu + "/" + jeu + ".csv"));
		String line = br.readLine();
		String[] data = null;
		while ((line = br.readLine()) != null) {
			data = line.split(";");
			if (jeu.equals("timeline")) {
				this.cartes.add(new Carte(data[0], Integer.parseInt(data[1]), data[2]));
			} else {
				this.cartes.add(new Carte(data[0], Double.parseDouble(data[1]), Integer.parseInt(data[2]),
						Integer.parseInt(data[3]), Double.parseDouble(data[4].replaceAll(",", ".")), data[5]));
			}
		}
		br.close();
		long t2 = System.currentTimeMillis();
		long time = t2 - t1;
		System.out.println("Temps de chargement des cartes : " + time + " millisecondes.");
	}

	public ArrayList<Carte> getCartes() {
		return cartes;
	}

}
