import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Fenetre {

	private int iterateur = 1;
	private JPanel plateau = new JPanel();
	private ArrayList<PanelMain> mains;
	private PanelMain mainEnCours;
	private JPanel joueursEnAttente;
	private JPanel plateauCentral;
	private JPanel scores;
	private Partie partie;
	private Terrain terrain;
	private JFrame frame;
	private MouseListener listenerMains;
	private int nbJoueurs;
	private int joueurActuel = 0;

	public void creerFenetre(Partie partie) throws IOException {

		// Creation de la fenêtre de jeu
		this.frame = new JFrame("Application");
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setSize(new Dimension(500, 300));
		int width = (int) screenSize.getWidth();
		int height = (int) screenSize.getHeight();

		// Creation d'une partie

		this.partie = partie;
		this.partie.setNbJoueurs(0);

		// Titre

		JLabel titre = new JLabel(partie.getVersionJeu(), SwingConstants.CENTER);
		titre.setPreferredSize(new Dimension(width, 50));
		titre.setFont(new Font(null, Font.BOLD, 26));

		frame.add(titre);

		// Premier menu

		JPanel menu1 = new JPanel();

		JLabel nombreJoueurs = new JLabel("Nombre de joueurs :");
		JTextField nbJ = new JTextField();
		nbJ.setPreferredSize(new Dimension(40, 20));
		JButton validerNbJoueurs = new JButton("Valider");
		JLabel nbFaux = new JLabel("Veuillez entrer un nombre correct de joueurs", SwingConstants.CENTER);
		nbFaux.setForeground(Color.red);

		nbFaux.setVisible(false);

		menu1.add(nombreJoueurs);
		menu1.add(nbJ);
		menu1.add(validerNbJoueurs);
		menu1.add(nbFaux);

		// Deuxième menu

		JPanel menu2 = new JPanel();

		JLabel joueur = new JLabel("Joueur 1 :");
		JLabel pseudo = new JLabel("Pseudo : ");
		JTextField psd = new JTextField();
		psd.setPreferredSize(new Dimension(100, 20));
		JLabel age = new JLabel("Age : ");
		JTextField ag = new JTextField();
		ag.setPreferredSize(new Dimension(40, 20));
		JButton validerJoueur = new JButton("Valider");

		menu2.add(joueur);
		menu2.add(pseudo);
		menu2.add(psd);
		menu2.add(age);
		menu2.add(ag);
		menu2.add(validerJoueur);

		menu2.setVisible(false);

		frame.add(menu1);
		frame.add(menu2);

		// Bouton permettant de quitter le premier menu et aller au deuxième

		validerNbJoueurs.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				nbJoueurs = Integer.parseInt(nbJ.getText());
				if (nbJoueurs < 2 || nbJoueurs > 8) {
					nbFaux.setVisible(true);
				} else {
					partie.setNbJoueurs(nbJoueurs);
					frame.remove(menu1);
					menu2.setVisible(true);
				}
				frame.repaint();
			}
		});

		// Bouton permettant de quitter le deuxième menu et aller au plateau de jeu

		validerJoueur.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Joueur j = new Joueur(psd.getText(), Integer.parseInt(ag.getText()));
				partie.ajouterJoueur(j);
				if (Fenetre.this.iterateur < partie.getNbJoueurs()) {
					Fenetre.this.iterateur++;
					psd.setText("");
					ag.setText("");
					joueur.setText("Joueur " + Fenetre.this.iterateur + " :");
					menu2.repaint();
				} else {
					frame.remove(menu2);
					Fenetre.this.plateau = Fenetre.this.creationPlateau();
					frame.add(plateau);
					frame.revalidate();
					frame.repaint();
					Fenetre.this.lancementPartie();
				}
			}
		});

		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.setLayout(new FlowLayout());
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	// Methode de creation du plateau de jeu

	public JPanel creationPlateau() {

		// Plateau de jeu
		JPanel panelPartie = new JPanel();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		panelPartie.setPreferredSize(new Dimension((int) screenSize.getWidth(), (int) screenSize.getHeight()));

		// On trie les joueurs par age et on distribue les cartes
		this.partie.jouerPartie();

		this.terrain = new Terrain();
		Carte carteTerrain = this.partie.getDeck().piocherCarte();
		this.terrain.ajouterCarte(0, carteTerrain);

		// Liste des joueurs
		ArrayList<Joueur> joueurs = this.partie.getJoueurs();

		// Joueurs en attente
		this.joueursEnAttente = new JPanel();
		joueursEnAttente.setPreferredSize(new Dimension(30 * 6 + 40, 72 * nbJoueurs));

		// Plateau central
		this.plateauCentral = new JPanel();
		plateauCentral.setPreferredSize(new Dimension(150 * 6 + 30, 320));
		ImageCarte imagePlateau = new ImageCarte(carteTerrain, carteTerrain.getImgVerso(), false);
		imagePlateau.setPreferredSize(new Dimension(100, 150));
		plateauCentral.add(imagePlateau);
		this.mains = new ArrayList<PanelMain>();

		// Score
		scores = new JPanel();
		scores.setPreferredSize(new Dimension(100, 300));
		scores.add(new JLabel("Score :"));

		// main en cours
		this.mainEnCours = new PanelMain();

		for (int j = 0; j < this.partie.getNbJoueurs(); j++) {
			mains.add(new PanelMain());
			mains.get(j).setJoueur(joueurs.get(j));
		}

		this.listenerMains = new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				PanelMain m = (PanelMain) e.getComponent();
				Component[] images = (Component[]) m.getComponents();
				int i = 1;
				boolean fin = false;
				Point p = e.getPoint();
				while (!fin && i < images.length) {
					if (images[i].getX() < p.getX() && images[i].getX() + images[i].getWidth() > p.getX()) {
						if (images[i].getY() < p.getY() && images[i].getY() + images[i].getHeight() > p.getY()) {
							fin = true;
							// faire action/choix
							Joueur joueur = m.getJoueur();
							ImageCarte img = (ImageCarte) images[i];
							Object[] options = new Object[terrain.getCartes().size() + 1];
							for (int z = 0; z < terrain.getCartes().size() + 1; z++) {
								options[z] = z;
							}
							int k = (int) JOptionPane.showInputDialog(null, "Ou voulez vous poser votre carte?",
									"Choix", JOptionPane.PLAIN_MESSAGE, null, options, 0);

							boolean posee = terrain.testerCarte(k, img.getCarte());
							if (posee) {
								JOptionPane.showMessageDialog(null, "VRAI");
								joueur.setScore(joueur.getScore() + 1);
								scores.removeAll();
								scores.add(new JLabel("Score :"));
								for (int j = 0; j < partie.getNbJoueurs(); j++) {
									JLabel scoreJoueur = new JLabel(partie.getJoueurs().get(j).getPseudo() + " :" + " "
											+ partie.getJoueurs().get(j).getScore());
									scoreJoueur.setPreferredSize(new Dimension(90, 20));
									scores.add(scoreJoueur);
								}
							} else {
								JOptionPane.showMessageDialog(null, "FAUX");
								Carte c = Fenetre.this.getPartie().getDeck().piocherCarte();
								joueur.piocherCarte(c);
							}
							joueur.getCartes().remove(img.getCarte());
							mainEnCours.remove(img);
							plateauCentral.removeAll();
							for (int j = 0; j < terrain.getCartes().size(); j++) {
								Carte c = terrain.getCartes().get(j);
								ImageCarte image = new ImageCarte(c, c.getImgVerso(), false);
								image.setPreferredSize(new Dimension(100, 150));
								plateauCentral.add(image);
							}
							frame.revalidate();
							frame.repaint();

							// Actualise les cartes du joueur
							for (PanelMain pm : mains) {
								pm.setPreferredSize(new Dimension(30 * 6 + 30, 45));
							}
							mains.get(joueurActuel).removeAll();
							ArrayList<Carte> listeCartes = joueur.getCartes();
							for (int l = 0; l < listeCartes.size(); l++) {
								img = new ImageCarte(listeCartes.get(l), listeCartes.get(l).getImgRecto(), true);
								mains.get(joueurActuel).add(img);
							}

							// Initialise tour du joueur suivant
							if (joueurActuel + 1 == nbJoueurs) {
								joueurActuel = 0;
							} else {
								joueurActuel++;
							}
							joueur = Fenetre.this.getPartie().getJoueurs().get(joueurActuel);

							// La main du joueur est mise en avant
							((PanelMain) Fenetre.this.getMains().get(joueurActuel)).setApercu(false);
							((PanelMain) Fenetre.this.getMains().get(joueurActuel))
									.setPreferredSize(new Dimension(30 * 6 + 30, 45));
							Fenetre.this.getMainEnCours().addMouseListener(listenerMains);
							Fenetre.this.getMainEnCours().setJoueur(joueur);
							Fenetre.this.getMainEnCours().removeAll();
							Fenetre.this.getMainEnCours().setApercu(false);

							// On affiche le pseudo du joueur en cours
							JLabel pseudoJoueur = new JLabel(joueur.getPseudo() + " :", SwingConstants.CENTER);
							pseudoJoueur.setPreferredSize(new Dimension(
									(int) Fenetre.this.getMainEnCours().getPreferredSize().getWidth(), 20));
							Fenetre.this.getMainEnCours().add(pseudoJoueur);

							// on récupere les cartes du joueur;
							listeCartes = joueur.getCartes();

							for (int j = 0; j < listeCartes.size(); j++) {
								img = new ImageCarte(listeCartes.get(j), listeCartes.get(j).getImgRecto(),
										Fenetre.this.getMains().get(joueurActuel).getApercu());
								Fenetre.this.getMainEnCours().add(img);
							}
							frame.revalidate();
							frame.repaint();
						}
					}
					i++;
				}
			}
		};
		// Creation de la main des joueurs
		for (int j = 0; j < this.partie.getNbJoueurs(); j++) {
			Joueur joueur = (this.getMains().get(j)).getJoueur();

			// on ajoute le joueur aux joueurs en attente
			joueursEnAttente.add(new JLabel(joueur.getPseudo() + " :"));

			// on ajoute le score du joueur
			JLabel scoreJoueur = new JLabel(joueur.getPseudo() + " :" + " " + joueur.getScore());
			scoreJoueur.setPreferredSize(new Dimension(90, 20));
			scores.add(scoreJoueur);

			// on affiche les cartes de la main du joueur
			ArrayList<Carte> listeCartes = joueur.getCartes();
			for (int k = 0; k < listeCartes.size(); k++) {
				ImageCarte img = new ImageCarte(listeCartes.get(k), listeCartes.get(k).getImgRecto(),
						mains.get(j).getApercu());
				mains.get(j).add(img);
			}
			joueursEnAttente.add(mains.get(j));
		}

		panelPartie.add(joueursEnAttente);
		panelPartie.add(plateauCentral);
		panelPartie.add(scores);
		// type partie pour cardline
		if (partie.getVersionJeu().equals("Cardline")) {
			JPanel typePartie = new JPanel();
			typePartie.setPreferredSize(new Dimension(400, 30));
			JComboBox<String> type = new JComboBox<String>();
			type.addItem("population");
			type.addItem("pib");
			type.addItem("pollution");
			type.addItem("superficie");
			JButton changerType = new JButton("Changer type");
			JLabel typeActuel = new JLabel("Type actuel : population");
			changerType.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					String t = (String) type.getSelectedItem();
					typeActuel.setText("Type actuel : " + t);
					terrain.changerType(t);
					plateauCentral.removeAll();
					for (int j = 0; j < terrain.getCartes().size(); j++) {
						Carte c = terrain.getCartes().get(j);
						ImageCarte image = new ImageCarte(c, c.getImgVerso(), false);
						image.setPreferredSize(new Dimension(100, 150));
						plateauCentral.add(image);
					}
				}
			});
			typePartie.add(type);
			typePartie.add(changerType);
			typePartie.add(typeActuel);
			panelPartie.add(typePartie);
		}
		panelPartie.add(mainEnCours);
		return panelPartie;
	}

	public void lancementPartie() {
		Joueur joueur = this.getPartie().getJoueurs().get(0);
		PanelMain main = (PanelMain) this.getMains().get(0);
		// La main du joueur est mise en avant
		main.setApercu(false);
		this.getMainEnCours().addMouseListener(this.listenerMains);
		this.getMainEnCours().setJoueur(joueur);
		this.getMainEnCours().removeAll();
		this.getMainEnCours().setApercu(false);

		// On affiche le pseudo du joueur en cours
		JLabel pseudoJoueur = new JLabel(joueur.getPseudo() + " :", SwingConstants.CENTER);
		pseudoJoueur.setPreferredSize(new Dimension((int) this.getMainEnCours().getPreferredSize().getWidth(), 20));
		this.getMainEnCours().add(pseudoJoueur);

		// on récupere les cartes du joueur;
		ArrayList<Carte> listeCartes = joueur.getCartes();

		for (int i = 0; i < listeCartes.size(); i++) {
			ImageCarte img = new ImageCarte(listeCartes.get(i), listeCartes.get(i).getImgRecto(), main.getApercu());
			this.getMainEnCours().add(img);
		}
		frame.revalidate();
		frame.repaint();
	}

	public int getIterateur() {
		return iterateur;
	}

	public JPanel getPlateau() {
		return plateau;
	}

	public ArrayList<PanelMain> getMains() {
		return mains;
	}

	public PanelMain getMainEnCours() {
		return mainEnCours;
	}

	public JPanel getJoueursEnAttente() {
		return joueursEnAttente;
	}

	public JPanel getPlateauCentral() {
		return plateauCentral;
	}

	public void setIterateur(int iterateur) {
		this.iterateur = iterateur;
	}

	public void setPlateau(JPanel plateau) {
		this.plateau = plateau;
	}

	public void setMains(ArrayList<PanelMain> mains) {
		this.mains = mains;
	}

	public void setMainEnCours(PanelMain mainEnCours) {
		this.mainEnCours = mainEnCours;
	}

	public void setJoueursEnAttente(JPanel joueursEnAttente) {
		this.joueursEnAttente = joueursEnAttente;
	}

	public void setPlateauCentral(JPanel plateauCentral) {
		this.plateauCentral = plateauCentral;
	}

	public Partie getPartie() {
		return partie;
	}

	public void setPartie(Partie partie) {
		this.partie = partie;
	}

	public Terrain getTerrain() {
		return terrain;
	}

	public void setTerrain(Terrain terrain) {
		this.terrain = terrain;
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public JPanel getScores() {
		return this.scores;
	}
}
