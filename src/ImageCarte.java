import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

public class ImageCarte extends JPanel {

	private Carte carte;
	private Image image;

	public ImageCarte(Carte c, Image i, boolean apercu) {
		super();
		if (apercu == true)
			this.setPreferredSize(new Dimension(30, 44));
		else
			this.setPreferredSize(new Dimension(150, 225));
		this.image = i;
		this.carte = c;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(this.image, 0, 0, this.getWidth(), this.getHeight(), null);
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public void setCarte(Carte carte) {
		this.carte = carte;
	}

	public Carte getCarte() {
		return carte;
	}

	public Image getImage() {
		return image;
	}

}
