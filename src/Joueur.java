import java.util.ArrayList;

public class Joueur {

	private String pseudo;
	private Integer age;
	private int num;
	private ArrayList<Carte> cartes;
	private int score;

	public Joueur(String p, int a) {
		this.pseudo = p;
		this.age = new Integer(a);
		this.cartes = new ArrayList<Carte>();
		this.score = 0;
	}

	/*
	 * M�thode qui permet de jouer une carte de la main du joueur, cette carte est
	 * supprim� de sa main
	 * 
	 * @param num�ro le num�ro de la carte jou�e
	 */
	public void jouerCarte(int numero) {
		this.cartes.remove(numero);
	}

	/*
	 * M�thode qui permet d'ajouter une carte dans la main du joueur
	 * 
	 * @param carte la carte qui va �tre ajout�e
	 */
	public void piocherCarte(Carte carte) {
		this.cartes.add(carte);
	}

	public Integer getAge() {
		return this.age;
	}

	public ArrayList<Carte> getCartes() {
		return cartes;
	}

	public void ajouterCarte(Carte c) {
		this.getCartes().add(c);
	}

	public void trierCartes() {
		for (int i = 0; i < this.getCartes().size(); i++)
			this.getCartes().get(i).setNumero(i);
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public void setCartes(ArrayList<Carte> cartes) {
		this.cartes = cartes;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

}
