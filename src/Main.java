import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Main {

	public static void main(String[] args) throws IOException {
		JFrame choixJeu = new JFrame();
		JPanel contenu = new JPanel();
		choixJeu.setSize(new Dimension(400, 400));
		JComboBox<String> choix = new JComboBox<String>();
		choix.addItem("Timeline");
		choix.addItem("Cardline");

		JButton validerChoix = new JButton("Valider");

		validerChoix.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				choixJeu.setVisible(false);
				String jeu = (String) choix.getSelectedItem();
				System.out.println(jeu);
				Deck deck = new Deck();
				Partie partie = new Partie(jeu, deck);
				try {
					deck.creerCartes(jeu.toLowerCase());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Fenetre fenetre = new Fenetre();
				try {
					fenetre.creerFenetre(partie);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		JLabel titre = new JLabel("Choix du jeu", SwingConstants.CENTER);
		titre.setPreferredSize(new Dimension(400, 50));
		titre.setFont(new Font(null, Font.BOLD, 26));
		contenu.add(titre);
		contenu.add(choix);
		contenu.add(validerChoix);
		choixJeu.add(contenu);
		choixJeu.setVisible(true);

	}
}
