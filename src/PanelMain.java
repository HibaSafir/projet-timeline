import java.awt.Dimension;

import javax.swing.JPanel;

public class PanelMain extends JPanel {

	private boolean apercu;
	private Joueur joueur;

	public PanelMain() {
		super();
		this.apercu = true;
		this.setPreferredSize(new Dimension(30 * 6 + 30, 45));
	}

	public boolean getApercu() {
		return this.apercu;
	}

	public void setApercu(boolean apercu) {
		this.apercu = apercu;
		if (apercu == false) {
			this.setPreferredSize(new Dimension(150 * 6 + 30, 250));
		}
	}

	public Joueur getJoueur() {
		return joueur;
	}

	public void setJoueur(Joueur joueur) {
		this.joueur = joueur;
	}
}
