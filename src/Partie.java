import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Comparator;

public class Partie {

	private int nombreJoueurs;
	private String versionJeu;
	private ArrayList<Joueur> joueurs;
	private Deck deck;

	public Partie(String vJ, Deck d) {
		this.versionJeu = vJ;
		this.joueurs = new ArrayList<Joueur>();
		this.deck = d;
	}

	/*
	 * M�thode qui donne le nombre de cartes de chaque joueur
	 * 
	 * @return le nombre de cartes
	 */
	public int cartesParJoueur() {
		int nbCartes = 0;
		switch (this.nombreJoueurs) {
		case 2:
		case 3:
			nbCartes = 6;
			break;
		case 4:
		case 5:
			nbCartes = 5;
			break;
		case 6:
		case 7:
		case 8:
			nbCartes = 4;
			break;
		}
		return nbCartes;
	}

	/*
	 * M�thode qui distribue les cartes aux joueurs
	 */
	public void distribuerCartes() {
		int nbCartes = this.cartesParJoueur();
		for (int i = 0; i < this.joueurs.size(); i++) {
			for (int j = 0; j < nbCartes; j++) {
				this.joueurs.get(i).piocherCarte(this.deck.piocherCarte());
			}
		}
	}

	/*
	 * M�thode qui ajoute un joueur dans la liste des joueurs
	 * 
	 * @param j le joueur que l'on veut ajouter
	 */
	public void ajouterJoueur(Joueur j) {
		this.joueurs.add(j);
	}

	/*
	 * M�thode qui trie les joueurs par �ge, le plus jeune en premier
	 */
	public void trierJoueursParAge() {
		this.joueurs.sort(new Comparator<Object>() {
			@Override
			public int compare(Object o1, Object o2) {
				return ((Joueur) o1).getAge().compareTo(((Joueur) o2).getAge());
			}
		});
	}

	/*
	 * M�thode qui correspond � une partie elle se termine quand un joueur n'a
	 * plus de carte dans sa main
	 */
	public void jouerPartie() {
		this.trierJoueursParAge();
		this.distribuerCartes();

	}

	/*
	 * M�thode qui correspond au tour d'un seul joueur
	 */
	public void tourDeJeu() {

	}

	public void sauvegarderPartie() throws IOException {
		// sauvegarde l'�tat du deck, des joueurs et le tour du joueur actuel
		ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(new File("data/test.txt")));
		o.writeObject(this);
		o.close();
	}

	public void setNbJoueurs(int nombre) {
		this.nombreJoueurs = nombre;
	}

	public int getNbJoueurs() {
		return this.nombreJoueurs;
	}

	public ArrayList<Joueur> getJoueurs() {
		return this.joueurs;
	}

	public String getVersionJeu() {
		return versionJeu;
	}

	public void setVersionJeu(String versionJeu) {
		this.versionJeu = versionJeu;
	}

	public Deck getDeck() {
		return deck;
	}

	public void setDeck(Deck deck) {
		this.deck = deck;
	}

	public void setJoueurs(ArrayList<Joueur> joueurs) {
		this.joueurs = joueurs;
	}

}
