import java.util.ArrayList;
import java.util.Comparator;

public class Terrain {

	private ArrayList<Carte> cartes;

	public Terrain() {
		this.cartes = new ArrayList<Carte>();
	}

	/*
	 * M�thode qui test si une carte peut �tre ajout� � l'endroit voulu
	 */
	public boolean testerCarte(int emplacement, Carte carte) {
		if (emplacement == 0) {
			Carte carteActuelle = this.getCartes().get(emplacement);
			int res = carteActuelle.comparerCartes(carte);
			if (res >= 0) {
				this.ajouterCarte(emplacement, carte);
				return true;
			}
		} else if (emplacement == this.cartes.size()) {
			Carte carteActuelle = this.getCartes().get(emplacement - 1);
			int res = carteActuelle.comparerCartes(carte);
			if (res <= 0) {
				this.ajouterCarte(emplacement, carte);
				return true;
			}
		} else {
			Carte carteAvant = this.getCartes().get(emplacement - 1);
			Carte carteApres = this.getCartes().get(emplacement);
			int res = carteAvant.comparerCartes(carte);
			int res2 = carteApres.comparerCartes(carte);
			if (res <= 0 && res2 >= 0) {
				this.ajouterCarte(emplacement, carte);
				return true;
			}
		}
		return false;
	}

	/*
	 * M�thode qui change le type de selection des cartes
	 */
	public void changerType(String type) {
		for (Carte c : this.cartes) {
			c.setTypeSelectionne(type);
		}
		this.trierCartes();
	}

	/*
	 * M�thode qui trie les cartes pour le jeu cardline
	 */
	public void trierCartes() {
		this.cartes.sort(new Comparator<Object>() {

			@Override
			public int compare(Object d1, Object d2) {
				return ((Carte) d1).comparerCartes((Carte) d2);
			}
		});
	}

	/*
	 * M�thode qui ajoute une carte � la position choisit dans la liste
	 * 
	 * @param emplacement l'emplacement choisit
	 * 
	 * @param carte la carte choisit
	 */
	public void ajouterCarte(int emplacement, Carte carte) {
		this.cartes.add(emplacement, carte);
	}

	public ArrayList<Carte> getCartes() {
		return cartes;
	}

}
