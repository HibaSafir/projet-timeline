import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class Tests {

	private Deck d;
	private Partie p;
	private Joueur j1, j2, j3;

	@Before
	public void initialisation() {
		d = new Deck();
		p = new Partie("Timeline", d);
		j1 = new Joueur("j1", 30);
		j2 = new Joueur("j2", 12);
		j3 = new Joueur("j3", 19);
		p.ajouterJoueur(j1);
		p.ajouterJoueur(j2);
		p.ajouterJoueur(j3);
	}

	@Test
	public void testPiocherCarte() throws IOException {
		d.creerCartes("timeline");
		Carte c = d.piocherCarte();
		assertFalse("La carte n'est plus dans le deck", d.getCartes().contains(c));
		Carte c2 = d.piocherCarte();
		assertFalse("Cartes diff�rentes", c.equals(c2));
	}

	@Test
	public void testAjouterJoueur() {
		Partie p2 = new Partie("Timeline", d);
		Joueur j1 = new Joueur("j1", 15);
		p2.ajouterJoueur(j1);
		assertTrue("Le joueur a �t� ajout�", p2.getJoueurs().contains(j1));
	}

	@Test
	public void testJoueurPlusJeuneCommence() {
		p.trierJoueursParAge();
		assertEquals("Le premier joueur est j2", p.getJoueurs().get(0), j2);
		assertEquals("Le premier joueur est j3", p.getJoueurs().get(1), j3);
		assertEquals("Le premier joueur est j1", p.getJoueurs().get(2), j1);
	}

	@Test
	public void testDistribuerCartes() throws IOException {
		d.creerCartes("timeline");
		p.distribuerCartes();
		int nb = p.cartesParJoueur();
		assertEquals("Le joueur 1 a le bon nombre de cartes", nb, p.getJoueurs().get(0).getCartes().size());
		assertEquals("Le joueur 2 a le bon nombre de cartes", nb, p.getJoueurs().get(1).getCartes().size());
		assertEquals("Le joueur 3 a le bon nombre de cartes", nb, p.getJoueurs().get(2).getCartes().size());
	}

	@Test
	public void testJouerCarteJoueur() {
		Joueur j = new Joueur("test", 18);
		Carte c = new Carte(200);
		j.piocherCarte(c);
		assertEquals("Le joueur a une carte", 1, j.getCartes().size());
		j.jouerCarte(0);
		assertEquals("Le joueur n'a plus de carte", 0, j.getCartes().size());
	}

	@Test
	public void testAjouterCarte() throws IOException {
		d.creerCartes("timeline");
		Terrain t = new Terrain();
		Carte c = d.piocherCarte();
		Carte c2 = d.piocherCarte();
		t.ajouterCarte(0, d.piocherCarte());
		t.ajouterCarte(1, c);
		assertEquals("La carte c est au bon endroit", c, t.getCartes().get(1));
		t.ajouterCarte(1, c2);
		assertEquals("La carte c2 est au bon endroit", c2, t.getCartes().get(1));
	}

	@Test
	public void testAjouterCarteSurTerrainApresVrai() {
		Terrain t = new Terrain();
		Carte c = new Carte(50);
		Carte c2 = new Carte(70);
		t.ajouterCarte(0, c);
		assertTrue("La carte devrait �tre ajout�e", t.testerCarte(1, c2));
		assertEquals("La carte est au bon endroit", c2, t.getCartes().get(1));
	}

	@Test
	public void testAjouterCarteSurTerrainApresFaux() {
		Terrain t = new Terrain();
		Carte c = new Carte(50);
		Carte c2 = new Carte(20);
		t.ajouterCarte(0, c);
		assertFalse("La carte ne devrait pas �tre ajout�e", t.testerCarte(1, c2));
		assertEquals("La carte n'est pas ajout�e", 1, t.getCartes().size());
	}

	@Test
	public void testAjouterCarteSurTerrainApresEgal() {
		Terrain t = new Terrain();
		Carte c = new Carte(50);
		Carte c2 = new Carte(50);
		t.ajouterCarte(0, c);
		assertTrue("La carte devrait �tre ajout�e", t.testerCarte(1, c2));
		assertEquals("La carte a �t� ajout�e", c2, t.getCartes().get(1));
	}

	@Test
	public void testAjouterCarteSurTerrainAvantVrai() {
		Terrain t = new Terrain();
		Carte c = new Carte(50);
		Carte c2 = new Carte(20);
		t.ajouterCarte(0, c);
		assertTrue("La carte devrait �tre ajout�e", t.testerCarte(0, c2));
		assertEquals("La carte est au bon endroit", c2, t.getCartes().get(0));
	}

	@Test
	public void testAjouterCarteSurTerrainAvantFaux() {
		Terrain t = new Terrain();
		Carte c = new Carte(50);
		Carte c2 = new Carte(60);
		t.ajouterCarte(0, c);
		assertFalse("La carte ne devrait pas �tre ajout�e", t.testerCarte(0, c2));
		assertEquals("La carte n'est pas ajout�e", 1, t.getCartes().size());
	}

	@Test
	public void testAjouterCarteSurTerrainAvantEgal() {
		Terrain t = new Terrain();
		Carte c = new Carte(50);
		Carte c2 = new Carte(50);
		t.ajouterCarte(0, c);
		assertTrue("La carte devrait �tre ajout�e", t.testerCarte(0, c2));
		assertEquals("La carte a �t� ajout�e", c2, t.getCartes().get(0));
	}

	@Test
	public void testAjouterCarteSurTerrainEntreVrai() {
		Terrain t = new Terrain();
		Carte c = new Carte(50);
		Carte c2 = new Carte(60);
		Carte c3 = new Carte(55);
		t.ajouterCarte(0, c);
		t.ajouterCarte(1, c2);
		assertTrue("La carte devrait �tre ajout�e", t.testerCarte(1, c3));
		assertEquals("La carte a �t� ajout�e", c3, t.getCartes().get(1));
	}

	@Test
	public void testAjouterCarteSurTerrainEntreFaux() {
		Terrain t = new Terrain();
		Carte c = new Carte(50);
		Carte c2 = new Carte(60);
		Carte c3 = new Carte(70);
		t.ajouterCarte(0, c);
		t.ajouterCarte(1, c2);
		assertFalse("La carte ne devrait pas �tre ajout�e", t.testerCarte(1, c3));
		assertEquals("La carte n'est pas ajout�e", 2, t.getCartes().size());
	}

	@Test
	public void testAjouterCarteSurTerrainEntreEgal() {
		Terrain t = new Terrain();
		Carte c = new Carte(50);
		Carte c2 = new Carte(60);
		Carte c3 = new Carte(60);
		t.ajouterCarte(0, c);
		t.ajouterCarte(1, c2);
		assertTrue("La carte devrait �tre ajout�e", t.testerCarte(1, c3));
		assertEquals("La carte a �t� ajout�e", c3, t.getCartes().get(1));
	}
}
